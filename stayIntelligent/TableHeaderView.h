//
//  TableHeaderView.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/29/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableHeaderView : UIView{
    
}

- (id)initWithTitle:(NSString*)title;

@end
