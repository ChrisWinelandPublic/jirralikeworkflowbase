//
//  IssuesMainViewController.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundViewController.h"
#import "PropertySelectionView.h"

@interface IssuesMainViewController : BackgroundViewController<UITableViewDataSource, UITableViewDelegate, PropertySelectionDelegate>{
    UITableView* issuesMainTableView;
    NSInteger currentlySelectedFilterRow;
}

@end
