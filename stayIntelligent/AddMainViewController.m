//
//  AddMainViewController.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AddMainViewController.h"
#import "TableHeaderView.h"
#import "AddIssueViewController.h"
#import "AddMessageViewController.h"

@interface AddMainViewController ()

@end

@implementation AddMainViewController

typedef enum{
    kIssueRow = 0,
    kMessageRow,
    kRowCount
}addMainTableViewRows;

- (id)init{
    if(self = [super init]){
        [self setNavBarLogoIcon];
        [self setTitle:NSLocalizedString(@"NewTitle", @"")];
        [[self tabBarItem]setImage:[UIImage imageNamed:@"plus.png"]];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    addMainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)
                                                        style:UITableViewStyleGrouped];
    [addMainTableView setBackgroundColor:[UIColor clearColor]];
    [addMainTableView setBackgroundView:nil];
    [addMainTableView setRowHeight:44.0f];
    [addMainTableView setSeparatorColor:[UIColor darkGrayColor]];
    [addMainTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [addMainTableView setDelegate:self];
    [addMainTableView setDataSource:self];
    [addMainTableView setShowsVerticalScrollIndicator:YES];
    [[self view]addSubview:addMainTableView];

}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case kIssueRow:
        {
            AddIssueViewController* addIssue = [[AddIssueViewController alloc]init];
            [[self navigationController]pushViewController:addIssue animated:YES];
        }
            break;
        case kMessageRow:
        {
            AddMessageViewController* addMessage = [[AddMessageViewController alloc]init];
            [[self navigationController]pushViewController:addMessage animated:YES];
        }
            break;
        default:
            break;
    }

}

#pragma mark - table vew data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return kRowCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * issueCellID = @"addCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:issueCellID];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:issueCellID];
    }
    switch (indexPath.row) {
        case kIssueRow:
        {
            [[cell textLabel]setText:@"Issue"];
        }
            break;
        case kMessageRow:
        {
            [[cell textLabel]setText:@"Message"];
        }
            break;
        default:
            break;
    }
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SECTIONHEADERHEIGHT;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TableHeaderView* tableHeader = [[TableHeaderView alloc]initWithTitle:@"Select Type"];
    return tableHeader;
}

@end
