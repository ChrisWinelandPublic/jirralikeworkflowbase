//
//  PropertyPickerViewController.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "PropertyPickerViewController.h"

@interface PropertyPickerViewController ()

@end

@implementation PropertyPickerViewController

- (id)init{
    return [self initWithCurrentSelectedProperty:@""];
}

- (id)initWithCurrentSelectedProperty:(NSString*)property{
    if(self = [super init]){
        [self setTitle:@"Property Selector"];
        currentlySelectedProperty = property;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    propertyPickerTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)
                                                      style:UITableViewStyleGrouped];
    [propertyPickerTableView setBackgroundColor:[UIColor clearColor]];
    [propertyPickerTableView setBackgroundView:nil];
    [propertyPickerTableView setSeparatorColor:[UIColor darkGrayColor]];
    [propertyPickerTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [propertyPickerTableView setDelegate:self];
    [propertyPickerTableView setDataSource:self];
    [propertyPickerTableView setShowsVerticalScrollIndicator:YES];
    [[self view]addSubview:propertyPickerTableView];
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - table view data souce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 12;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *issueCellID = @"propertyPickerCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:issueCellID];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:issueCellID];
    }
    
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, SECTIONHEADERHEIGHT)];
    [headerView setBackgroundColor:BACKGROUNDCONTROASTCOLOR];
    UILabel* headerMessage = [[UILabel alloc]initWithFrame:CGRectMake(9, 9, screenWidth-18, 18)];
    [headerMessage setBackgroundColor:[UIColor clearColor]];
    [headerMessage setTextAlignment:NSTextAlignmentCenter];
    [headerMessage setText:@"Properties"];
    [headerMessage setTextColor:[UIColor blackColor]];
    [headerMessage setFont:SECTIONHEADERFONT];
    [headerMessage setNumberOfLines:0];
    [headerView addSubview:headerMessage];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SECTIONHEADERHEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 55.0f;
}

@end