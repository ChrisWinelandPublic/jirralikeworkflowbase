//
//  main.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
