//
//  constants.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/23/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#define PROPERTYBANNERHEIGHT 55.0f
#define STATUSBARHEIGHT 22.0f
#define NAVBARHEIGHT 44.0f
#define TABBARHEIGH 44.0f
#define STANDARDBUFFER 6.0f


#define USABLESCREENSTARTINGYLOCATION STATUSBARHEIGHT + NAVBARHEIGHT - 2
#define USABLESCREENSTARTINGYLOCATIONWITHPROPERTYBANNER USABLESCREENSTARTINGYLOCATION + PROPERTYBANNERHEIGHT + 1

#define BACKGROUNDCONTROASTCOLOR [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:1.0f]
#define BACKGROUNDCOLOR [UIColor colorWithRed:.95f green:.95f blue:.95f alpha:1.0f]

#define SECTIONHEADERHEIGHT 36
#define SECTIONHEADERFONT [UIFont systemFontOfSize:14]