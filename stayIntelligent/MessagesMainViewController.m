//
//  MessagesMainViewController.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "MessagesMainViewController.h"
#import "TableHeaderView.h"

@interface MessagesMainViewController ()

@end

@implementation MessagesMainViewController

typedef enum{
    kMessageFilterSection = 0,
    kMessageCategorySection,
    kMessageSectionCount
}messagesMainSections;

typedef enum{
    kLocationMessagesRow = 0,
    kPersonMessagesRow,
    kNewMessagesRow,
    kMessageFilterRowCount
}messagesMainRows;

- (id)init{
    if(self = [super init]){
        [self setNavBarLogoIcon];
        [self setTitle:NSLocalizedString(@"MessagesTitle", @"")];
        [[self tabBarItem]setImage:[UIImage imageNamed:@"message.png"]];
        currentlySelectedFilterRow = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    messagesMainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)
                                                      style:UITableViewStyleGrouped];
    [messagesMainTableView setBackgroundColor:[UIColor clearColor]];
    [messagesMainTableView setBackgroundView:nil];
    [messagesMainTableView setRowHeight:44.0f];
    [messagesMainTableView setSeparatorColor:[UIColor darkGrayColor]];
    [messagesMainTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [messagesMainTableView setDelegate:self];
    [messagesMainTableView setDataSource:self];
    [messagesMainTableView setShowsVerticalScrollIndicator:YES];
    [[self view]addSubview:messagesMainTableView];
}

#pragma mark - UITableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kMessageFilterSection:
        {
            if(indexPath.row == currentlySelectedFilterRow) return;
            currentlySelectedFilterRow = indexPath.row;
            [self filterSwitched];
        }
            break;
        case kMessageCategorySection:
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
            break;
            
        default:
            break;
    }

}

#pragma mark - UITableView data source methods

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TableHeaderView* tableHeader;
    switch (section) {
        case kMessageFilterSection:
        {
            tableHeader = [[TableHeaderView alloc]initWithTitle:@"Message Filters"];
        }
            break;
        case kMessageCategorySection:
        {
            tableHeader = [[TableHeaderView alloc]initWithTitle:@"Message Group"];
        }
            break;
        default:
            break;
    }
    return tableHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SECTIONHEADERHEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section == kMessageCategorySection){
        return 55.0f;
    }else {
        return 0.0000001f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case kMessageFilterSection:
        {
            return kMessageFilterRowCount;
        }
            break;
        case kMessageCategorySection:
        {
            return 8;
        }
            break;
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return kMessageSectionCount;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kMessageFilterSection:
        {
            return [self cellForMessageFilterSectionAtIndexPath:indexPath];
        }
            break;
        case kMessageCategorySection:
        {
            return [self cellForMessageCategorySectionAtIndexPath:indexPath];
        }
            break;
        default:
        {
            return nil;
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kMessageFilterSection:
        {
            return 40.0f;
        }
            break;
        case kMessageCategorySection:
        {
            return 55.0f;
        }
            break;
        default:
        {
            return 0.0f;
        }
            break;
    }
}

#pragma mark issuesMainTableView helper methods

- (UITableViewCell*)cellForMessageFilterSectionAtIndexPath:(NSIndexPath*)indexPath{
    static NSString * issueFilterCellID = @"messageFilterCellID";
    UITableViewCell *cell = [messagesMainTableView dequeueReusableCellWithIdentifier:issueFilterCellID];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:issueFilterCellID];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    switch (indexPath.row) {
        case kLocationMessagesRow:
        {
            [[cell textLabel]setText:@"Location"];
            [[cell imageView]setImage:[UIImage imageNamed:@"location.png"]];
        }
            break;
        case kPersonMessagesRow:
        {
            [[cell textLabel]setText:@"Person"];
            [[cell imageView]setImage:[UIImage imageNamed:@"user.png"]];
        }
            break;
        case kNewMessagesRow:
        {
            [[cell textLabel]setText:@"New"];
            [[cell imageView]setImage:[UIImage imageNamed:@"newMessage.png"]];
        }
            break;
        default:
        {
            
        }
            break;
    }
    
    if(indexPath.row == currentlySelectedFilterRow){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}

- (UITableViewCell*)cellForMessageCategorySectionAtIndexPath:(NSIndexPath*)indexPath{
    static NSString * issueCellID = @"messageCellID";
    UITableViewCell *cell = [messagesMainTableView dequeueReusableCellWithIdentifier:issueCellID];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:issueCellID];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (void)filterSwitched{
    switch (currentlySelectedFilterRow) {
        case kLocationMessagesRow:
        {
            [self setUpLocationMessageData];
        }
            break;
        case kPersonMessagesRow:
        {
            [self setUpPersonMessageData];
        }
            break;
        case kNewMessagesRow:
        {
            [self setUpNewMessageData];
        }
            break;
        default:
        {
            
        }
            break;
    }
    [messagesMainTableView reloadData];
}

#pragma mark - helper functions

- (void)setUpLocationMessageData{
    
}

- (void)setUpPersonMessageData{
    
}

- (void)setUpNewMessageData{
    
}

@end