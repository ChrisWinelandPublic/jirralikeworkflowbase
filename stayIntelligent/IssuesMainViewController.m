//
//  IssuesMainViewController.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "IssuesMainViewController.h"
#import "PropertyPickerViewController.h"
#import "TableHeaderView.h"

@interface IssuesMainViewController ()

@end

@implementation IssuesMainViewController

typedef enum{
    kIssueFilterSection = 0,
    kIssueSection,
    kSectionCount
}issuesMainTableViewSections;

typedef enum{
    kMyIssueRow = 0,
    kAllIssueRow,
    kIssueFilterRowCount
}issueFilterRows;

- (id)init{
    if(self = [super init]){
        [self setNavBarLogoIcon];
        [self setTitle:NSLocalizedString(@"HomeTitle", @"")];
        [[self tabBarItem]setImage:[UIImage imageNamed:@"home.png"]];
        currentlySelectedFilterRow = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationItem]setTitle:@"StayIntelligent"];
    
    PropertySelectionView* propertySelector = [[PropertySelectionView alloc]init];
    [propertySelector setDelegate:self];
    [[self view]addSubview:propertySelector];
    
    issuesMainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, USABLESCREENSTARTINGYLOCATIONWITHPROPERTYBANNER, screenWidth, screenHightMinusPropertySelector)
                                                      style:UITableViewStyleGrouped];
    [issuesMainTableView setBackgroundColor:[UIColor clearColor]];
    [issuesMainTableView setBackgroundView:nil];
    [issuesMainTableView setSeparatorColor:[UIColor darkGrayColor]];
    [issuesMainTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [issuesMainTableView setDelegate:self];
    [issuesMainTableView setDataSource:self];
    [issuesMainTableView setShowsVerticalScrollIndicator:YES];
    [[self view]addSubview:issuesMainTableView];
    
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TableHeaderView* tableHeader;
    switch (section) {
        case kIssueFilterSection:
        {
            tableHeader = [[TableHeaderView alloc]initWithTitle:@"Issue Filters"];
        }
            break;
        case kIssueSection:
        {
            tableHeader = [[TableHeaderView alloc]initWithTitle:@"Issues"];
        }
        default:
            break;
    }
    return tableHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SECTIONHEADERHEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section == kIssueSection){
        return 55.0f;
    } else {
        return 0.0000001f;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kIssueFilterSection:
        {
            if(indexPath.row == currentlySelectedFilterRow) return;
            currentlySelectedFilterRow = indexPath.row;
            [self filterSwitched];
        }
            break;
        case kIssueSection:
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case kIssueFilterSection:
        {
            return kIssueFilterRowCount;
        }
            break;
        case kIssueSection:
        {
            return 8;
        }
            break;
        default:
        {
            return 0;
        }
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return kSectionCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kIssueFilterSection:
        {
            return 40.0f;
        }
            break;
        case kIssueSection:
        {
            return 55.0f;
        }
            break;
        default:
        {
            return 0.0f;
        }
            break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case kIssueFilterSection:
        {
            return [self cellForIssueFilterSectionAtIndexPath:indexPath];
        }
            break;
        case kIssueSection:
        {
            return [self cellForIssueSectionAtIndexPath:indexPath];
        }
            break;
        default:
        {
            return nil;
        }
            break;
    }
}

#pragma mark issuesMainTableView helper methods

- (UITableViewCell*)cellForIssueFilterSectionAtIndexPath:(NSIndexPath*)indexPath{
    static NSString * issueFilterCellID = @"issueFilterCellID";
    UITableViewCell *cell = [issuesMainTableView dequeueReusableCellWithIdentifier:issueFilterCellID];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:issueFilterCellID];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    switch (indexPath.row) {
        case kMyIssueRow:
        {
            [[cell textLabel]setText:@"My Open Issues"];
            [[cell imageView]setImage:[UIImage imageNamed:@"user.png"]];
        }
            break;
        case kAllIssueRow:
        {
            [[cell textLabel]setText:@"All Open Issues"];
            [[cell imageView]setImage:[UIImage imageNamed:@"group.png"]];
        }
            break;
        default:
        {
            
        }
            break;
    }
    
    if(indexPath.row == currentlySelectedFilterRow){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}

- (UITableViewCell*)cellForIssueSectionAtIndexPath:(NSIndexPath*)indexPath{
    static NSString * issueCellID = @"issueCellID";
    UITableViewCell *cell = [issuesMainTableView dequeueReusableCellWithIdentifier:issueCellID];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:issueCellID];
    }
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

- (void)filterSwitched{
    switch (currentlySelectedFilterRow) {
        case kMyIssueRow:
        {
            [self setUpMyIssuesData];
        }
            break;
        case kAllIssueRow:
        {
            [self setUpAllIsuesData];
        }
            break;
        default:
        {
            
        }
            break;
    }
    [issuesMainTableView reloadData];
}

#pragma mark - helper methods

- (void)setUpAllIsuesData{
    
}

- (void)setUpMyIssuesData{
    
}

- (void)propertyWasSelected{
    PropertyPickerViewController* propertyPicker = [[PropertyPickerViewController alloc]initWithCurrentSelectedProperty:[[UserContext singleton] currentlySelectedProperty]];
    [[self navigationController]pushViewController:propertyPicker animated:YES];
}

@end
