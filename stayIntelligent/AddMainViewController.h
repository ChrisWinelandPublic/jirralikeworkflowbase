//
//  AddMainViewController.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundViewController.h"

@interface AddMainViewController : BackgroundViewController <UITableViewDataSource, UITableViewDelegate>{
    UITableView* addMainTableView;
}

@end
