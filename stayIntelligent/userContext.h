//
//  userContext.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/23/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserContext : NSObject {
    NSArray* avalibleProperties;
    NSMutableArray* issues;
    NSMutableArray* messages;
}

@property BOOL isDisplayingPosack;
@property (nonatomic, strong) NSString* currentlySelectedProperty;

+ (id)singleton;

@end
