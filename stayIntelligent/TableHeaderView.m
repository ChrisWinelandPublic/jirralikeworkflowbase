//
//  TableHeaderView.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/29/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "TableHeaderView.h"
#import "constants.h"

@implementation TableHeaderView

- (id)initWithTitle:(NSString*)title{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if(self = [super initWithFrame:CGRectMake(0, 0, screenRect.size.width, SECTIONHEADERHEIGHT)]){
        [self setBackgroundColor:BACKGROUNDCONTROASTCOLOR];
        UILabel* headerMessage = [[UILabel alloc]initWithFrame:CGRectMake(9, 9, screenRect.size.width-18, 18)];
        [headerMessage setBackgroundColor:[UIColor clearColor]];
        [headerMessage setTextAlignment:NSTextAlignmentCenter];
        [headerMessage setText:title];
        [headerMessage setTextColor:[UIColor blackColor]];
        [headerMessage setFont:SECTIONHEADERFONT];
        [headerMessage setNumberOfLines:0];
        [self addSubview:headerMessage];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithTitle:@""];
}

@end