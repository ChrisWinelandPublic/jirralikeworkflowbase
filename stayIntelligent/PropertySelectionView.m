//
//  PropertySelectionView.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/23/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "PropertySelectionView.h"
#import "constants.h"

@implementation PropertySelectionView

@synthesize delegate;

- (id)init{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return [self initWithFrame:CGRectMake(0, USABLESCREENSTARTINGYLOCATION, screenRect.size.width, PROPERTYBANNERHEIGHT)];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:BACKGROUNDCONTROASTCOLOR];
        propertyLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [propertyLable setTextColor:[UIColor blackColor]];
        [propertyLable setFont:[UIFont systemFontOfSize:18.0f]];
        [propertyLable setNumberOfLines:1];
        [propertyLable setTextAlignment:NSTextAlignmentCenter];
        [propertyLable setText:@"Property - Comfort Inn Palo Alto"];
        [self addSubview:propertyLable];
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(propertySelectorWasTapped:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)propertySelectorWasTapped:(id)sender{
    [delegate propertyWasSelected];
}

@end
