//
//  AddMessageViewController.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/29/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AddMessageViewController.h"

@interface AddMessageViewController ()

@end

@implementation AddMessageViewController

- (id)init{
    if(self = [super init]){
        [[self navigationItem]setTitle:@"Create Message"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

@end
