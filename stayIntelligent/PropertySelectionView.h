//
//  PropertySelectionView.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/23/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PropertySelectionDelegate;

@interface PropertySelectionView : UIView {
    UILabel* propertyLable;
}

@property (nonatomic, weak) id<PropertySelectionDelegate> delegate;


@end

@protocol PropertySelectionDelegate <NSObject>

- (void)propertyWasSelected;

@end