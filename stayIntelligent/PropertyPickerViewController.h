//
//  PropertyPickerViewController.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "BackgroundViewController.h"

@interface PropertyPickerViewController : BackgroundViewController<UITableViewDataSource, UITableViewDelegate>{
    NSString* currentlySelectedProperty;
    UITableView* propertyPickerTableView;
}

- (id)initWithCurrentSelectedProperty:(NSString*)property;

@end