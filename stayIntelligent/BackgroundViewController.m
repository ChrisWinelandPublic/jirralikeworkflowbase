//
//  BackgroundViewController.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "BackgroundViewController.h"

@interface BackgroundViewController ()

@end

@implementation BackgroundViewController

- (id)init{
    if(self = [super init]){
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        screenWidth = screenRect.size.width;
        screenHeight = screenRect.size.height;
        screenHightMinusPropertySelector = screenHeight-172;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self view]setBackgroundColor:BACKGROUNDCOLOR];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@""
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:nil
                                                                           action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"exit.png"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(signOutClicked:)];
}

#pragma mark - post acknolagement accessor

-(void)showPosackWithMessage:(NSString*)message{
    if([[UserContext singleton]isDisplayingPosack]) return;
    [[UserContext singleton]setIsDisplayingPosack:YES];
    PostAcknolagement* posack = [[PostAcknolagement alloc]initWithMessage:message];
    [[self view]addSubview:posack];
    [posack setDelegate:self];
    [posack moveDownAction];
}

#pragma mark - post acknolagement delegate

- (void)postAcknoledgmentFinnishedMoveDownAction:(PostAcknolagement *)posack{
    
}

- (void)postAcknoledgmentFinnishedMoveUpAction:(PostAcknolagement *)posack{
    [[UserContext singleton]setIsDisplayingPosack:NO];
}

-(void)postAcknoledgmentWasTapped:(PostAcknolagement *)posack{
    [posack moveUpAction];
}

#pragma mark - uiaction sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [self signOutAction];
    }
}

#pragma mark - helperfunctions

- (void)setNavBarLogoIcon{
    UIImageView* logo = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"StayintelligentLogo.png"]];
    [[self navigationItem]setTitleView:logo];
    [[[self navigationItem]titleView]setFrame:CGRectMake(2, 2, 80, 40)];
}

- (void)signOutClicked:(id)sender{
    UIActionSheet* signOutActionSheet = [[UIActionSheet alloc]initWithTitle:@"Are you sure you would like to sign out?" delegate:self cancelButtonTitle:@"NO" destructiveButtonTitle:@"YES" otherButtonTitles:nil, nil];
    [signOutActionSheet showInView:[self view]];
}

- (void)signOutAction{
    
}

@end
