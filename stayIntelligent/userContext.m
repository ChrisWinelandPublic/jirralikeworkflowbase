//
//  userContext.m
//  stayIntelligent
//
//  Created by Chris Wineland on 5/23/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "userContext.h"

@implementation UserContext

- (id)init{
    if(self = [super init]){
        self.isDisplayingPosack = NO;
        self.currentlySelectedProperty = @"";
    }
    return self;
}

+ (id)singleton{
    static UserContext *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

@end
