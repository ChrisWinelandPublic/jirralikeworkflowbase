//
//  PostAcknolagement.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PostAcknolagementDelegate;

@interface PostAcknolagement : UIView{
    BOOL wasTaped;
}

@property (nonatomic, weak) id<PostAcknolagementDelegate>delegate;

- (id)initWithMessage:(NSString*)message;
- (void)moveDownAction;
- (void)moveUpAction;


@end

@protocol PostAcknolagementDelegate <NSObject>

- (void)postAcknoledgmentFinnishedMoveUpAction:(PostAcknolagement*)posack;
- (void)postAcknoledgmentFinnishedMoveDownAction:(PostAcknolagement*)posack;
- (void)postAcknoledgmentWasTapped:(PostAcknolagement*)posack;
@end