//
//  BackgroundViewController.h
//  stayIntelligent
//
//  Created by Chris Wineland on 5/21/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#import "PostAcknolagement.h"
#import "userContext.h"

@interface BackgroundViewController : UIViewController<PostAcknolagementDelegate, UIActionSheetDelegate>{
    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat screenHightMinusPropertySelector;
}

-(void)showPosackWithMessage:(NSString*)message;
-(void)setNavBarLogoIcon;

@end
